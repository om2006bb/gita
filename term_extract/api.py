__author__ = 'omprakash'

import trollius as asyncio
import json
from topia.termextract import extract

extractor = extract.TermExtractor()
extractor.filter = extract.permissiveFilter

imp_tag = ['JJ','JJR', 'JJS', 'VBG', 'VBN', 'NNS', 'NN', 'VB']
useless_tag = ['CC', 'CD', 'DT', 'EX', 'FW', 'IN', 'MD', 'NNS', 'NNP', 'PRP', 'PRP$', 'RB', 'RBS', 'TO', 'VBP', 'VBD', 'VBZ', 'WP', 'WRB', 'WDT', 'WP$']


class EchoServerClientProtocol(asyncio.Protocol):
    def connection_made(self, transport):
        peername = transport.get_extra_info('peername')
        print('Connection from {}'.format(peername))
        self.transport = transport

    def _get_keywords(self, message):
        words = extractor.tagger(message)
        words = [word for word in words if word[1] in imp_tag]
        return words

    def data_received(self, data):
        r_data = data.decode()
        data = json.loads(r_data)
        print("data_received" + r_data)
        data["message"] = self._get_keywords(data["message"])
        data = json.dumps(data)
        print("data_sent" + data)
        self.transport.write(data.encode())

    def connection_lost(self, exc):
        print('The server closed the connection')
        print('Stop the event loop')

loop = asyncio.get_event_loop()
# Each client connection will create a new protocol instance
coro = loop.create_server(EchoServerClientProtocol, '127.0.0.1', 8887)
server = loop.run_until_complete(coro)

# Serve requests until Ctrl+C is pressed
print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

# Close the server
server.close()
loop.run_until_complete(server.wait_closed())
loop.close()