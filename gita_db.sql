CREATE DATABASE gita_db;

drop table gita_data;
drop table gita_keywords_mapping ;
drop table question;

CREATE TABLE gita_data
(
    id int primary key,
    chapter_num int not null,
    text_num int not null,
    text text not null,
    translation text not null,
    purport text
);


CREATE TABLE gita_keywords_mapping
(
    gita_data_id int not null,
    keyword text not null,
    created_by text default null,
    sys_score float default 0,
    sys_score_count int default 0,
    user_score float default 0,
    user_score_count int default 0,
    score float default 0,
    created timestamp default now()
);


CREATE TABLE question
(
    id SERIAL primary key,
    question text,
    user_id text,
    gita_id int,
    feedback_id int,
    extra_data json,
    created timestamp default now(),
    updated timestamp default now()
);

CREATE TABLE scores
(
    id SERIAL primary key,
    max_score float,
    type int default 1
);

insert into scores(max_score, type) values(-1, 1), (-1, 2);


delete from gita_data; delete from gita_keywords_mapping ;