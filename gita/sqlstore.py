__author__ = 'omprakash'

from cauldron import PostgresStore
import json
from datetime import datetime

TABLE = 'gita_data'
COLUMNS = ['id', 'chapter_num']

KEYWORD_MAPPING = 'gita_keywords_mapping'
QUESTION_TABLE = 'question'
SCORES_TABLE = 'scores'


config = json.load(open('../config.json'))


class GitaRepository(PostgresStore):

    @classmethod
    def connect(cls):
        PostgresStore.connect(config['POSTGRES_DB'], config['POSTGRES_USER'], config['POSTGRES_PASS'],
                          config['POSTGRES_HOST'], config['POSTGRES_PORT'])

    @classmethod
    def insert_chapter_text(cls,  row):
        row = yield from cls.insert(TABLE, values=row)
        row = cls.get_dict_from_recort(row) if row else None
        return row

    @classmethod
    def insert_gita_data_keyword(cls, row):
        row = yield from cls.insert(KEYWORD_MAPPING, values=row)
        row = cls.get_dict_from_recort(row) if row else None
        return row

    @classmethod
    def update_max_score(cls, max_score, type=1):
        row = yield from cls.update(SCORES_TABLE, values={'max_score': max_score}, where_keys=[{'type': ('=', type)}])
        row = cls.get_dict_from_recort(row) if row else None
        return row

    @classmethod
    def get_max_scores(cls):
        rows = yield from cls.select(SCORES_TABLE, order_by='id')
        scores = {}
        for row in rows:
            scores[row.type] = float(row.max_score)
        return scores

    @classmethod
    def insert_question(cls, question, user_id, gita_id, extra_data, feedback_id = 0):
        row = {'question': question, 'user_id': user_id, 'gita_id': gita_id, 'extra_data': json.dumps(extra_data) ,'feedback_id': feedback_id}
        row = yield from cls.insert(QUESTION_TABLE, values=row)
        row = cls.get_dict_from_recort(row) if row else None
        return row

    @classmethod
    def save_feedback(cls, question_id, user_id, feedback_id):
        sql = """Update {} SET feedback_id=%s, updated=%s WHERE id=%s and user_id=%s returning *""".format(QUESTION_TABLE)
        rows = yield from cls.raw_sql(sql, (feedback_id, datetime.now(), question_id, user_id))
        row = cls.get_dict_from_recort(rows[0]) if rows else None
        return row

    @classmethod
    def update_score(cls, gita_id, keyword, score):
        sql = """Update {} SET score=%s WHERE gita_data_id=%s and keyword=%s returning *""".format(KEYWORD_MAPPING)
        yield from cls.raw_sql(sql, (score, gita_id, keyword))

    @classmethod
    def update_user_score(cls, gita_id, keywords, user_score):
        sql = """Update {} SET user_score = user_score + {}, user_score_count = user_score_count + 1
        WHERE gita_data_id=%s and keyword in ('{}') returning *""".format(KEYWORD_MAPPING, user_score, "', '".join(keywords))
        yield from cls.raw_sql(sql, (gita_id,))

    @classmethod
    def get_all_data(cls, offset=0, limit=1000):
        rows = yield from cls.select(TABLE, order_by='id', offset=0, limit=limit)
        rows = [cls.get_dict_from_recort(row) if row else None for row in rows]
        return rows

    @staticmethod
    def get_dict_from_recort(record):
        dict_obj = {}
        for field in record._fields:
            dict_obj[field] = getattr(record, field)
            if type(dict_obj[field]) == datetime:
                del dict_obj[field]
        return dict_obj

    @classmethod
    def get_gita_data_by_id(cls, id):
        rows = yield from cls.select(TABLE, order_by='id', where_keys=[{"id":('=',id)}])
        row = dict(cls.get_dict_from_recort(rows[0])) if rows else None
        return row

    @classmethod
    def get_gita_keywords_by_id(cls, id, keywords:list=''):
        sql = """Select * from gita_keywords_mapping WHERE keyword in ('{}') and gita_data_id = {}""".format("','".join(keywords), id)
        rows = yield from cls.raw_sql(sql, ())
        rows = [dict(cls.get_dict_from_recort(row)) for row in rows]
        return rows

    @classmethod
    def search_best_gita_data(cls, keywords:list):
        sql = """Select gita_data_id, sum(score) as count from gita_keywords_mapping
         WHERE keyword in ('{}') group by gita_data_id order by count desc LIMIT 1""".format("','".join(keywords))
        rows = yield from cls.raw_sql(sql, ())
        print(rows)
        id = rows[0].gita_data_id if rows else None
        return id


