import asyncio

from start_server import insert_data_into_db, generate_keywords, GitaRepository

if __name__ == '__main__':
    GitaRepository.connect()
    asyncio.get_event_loop().run_until_complete(insert_data_into_db())
    asyncio.get_event_loop().run_until_complete(generate_keywords())
