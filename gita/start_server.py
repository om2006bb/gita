__author__ = 'om'
import re
from sqlstore import GitaRepository
import asyncio
from aiohttp.web import Request
from again.utils import unique_hex
from asyncio import Future
import json
from stemming.porter2 import stem
from collections import defaultdict
import asyncio
import json
from aiohttp import web
from unidecode import unidecode
from PyDictionary import PyDictionary
dictionary=PyDictionary()


regexp = re.compile(r'(\d)+(\s)*(\)\))')

def print_log(message):
    #print(message)
    pass


def print_mantra(data, chapter, text):
    text = data[chapter-1][text-1]
    print_log('\n')
    print_log(''.join(text['TEXT']))
    print_log('\n')
    print_log(text['TRANSLATION'])


def text_common_filter(text):
    result = unidecode(text.replace('\n', ' ').strip())
    result = ' '.join(result.split())
    return result

def prepare_data(data):
    values = []
    i=0
    for chapter_num, chapter_data in enumerate(data):
        for text_num, text_data in enumerate(chapter_data):
            #print_log(str(chapter_num) + "," + str(text_num))
            if chapter_num==0 and text_num==15:
                pass
            if text_data:
                i+=1
                value = {
                    "id": i,
                    "chapter_num": chapter_num+1,
                    "text_num": text_num+1,
                    "text": '</br>'.join(text_data['TEXT']),
                    "translation": text_data['TRANSLATION']
                }
                if text_data.get('PURPORT'):
                    value['purport'] = text_data['PURPORT']
                values.append(value)
    return values


def generate_data():
    count = 0

    chapter_num = 0
    text_num = 0

    ignore_word = 'Copyright ©'
    in_translation = False
    in_purport = False
    traslation = ''
    purport = ''
    data = []
    multiple = 0

    input_lines = []
    with open('../data/gita.txt', encoding='utf-8') as f:
        for lineno, line in enumerate(f, 1):
            #line = line.strip(' ').strip('\n')
            input_lines.append(line + ' ')

    size = len(input_lines)
    i = 0
    while i < size:
        #print_log(i)
        line = input_lines[i]
        i+=1

        if in_translation:
            if 'PURPORT' in line or 'TEXT' in line or 'CHAPTER' in line:
                in_translation = False
                if 'PURPORT' in line:
                    in_purport = True
                data[chapter_num-1][text_num-1]['TRANSLATION'] = text_common_filter(traslation)
                traslation = ''
            else:
                if line and not line.startswith(ignore_word):
                    traslation+=line

            if 'TEXT' not in line and 'CHAPTER' not in line:
                continue

        if in_purport:
            if 'TEXT' in line or 'CHAPTER' in line:
                in_purport = False
                data[chapter_num-1][text_num-1]['PURPORT'] = text_common_filter(purport)
                purport = ''
            else:
                if line and not line.startswith(ignore_word):
                    purport += line

            if 'TEXT' not in line and 'CHAPTER' not in line:
                continue

        if ('CHAPTER '+str(chapter_num+1)) in line:
            text_num=0
            chapter_num+=1
            data.append([])
            #print_log('CHAPTER' + str(chapter_num))
        elif ('TEXT') in line:
            split_line = line.split('-')
            if len(split_line)<=1:
                split_line = input_lines[i].split('–')
            if len(split_line)<=1:
                split_line = input_lines[i+1].split('–')

            multiple = 0
            if len(split_line)>1:
                try:
                    num = int(split_line[1].strip())
                    multiple = num-text_num
                except ValueError:
                    pass

            if multiple:
                while(text_num < num):
                    text_num+=1
                    data[chapter_num-1].append([])
            else:
                text_num+=1
                data[chapter_num-1].append([])

            #print_log('TEXT' + str(text_num))
            pass
        elif 'SYNONYMS' in line:
            def get_prev_line(i, input_lines):
                count = 0
                list = []
                while not regexp.search(input_lines[i]):
                    if input_lines[i] and not input_lines[i].startswith(ignore_word):
                        list.insert(0, input_lines[i])
                        count+=1
                    i-=1
                return list

            if multiple:
                data[chapter_num-1][text_num-1]={
                        "TEXT": [text_common_filter(word) for word in list(get_prev_line(i-2, input_lines))]
                }
            else:
                data[chapter_num-1][text_num-1]={
                    "TEXT": [text_common_filter(word) for word in list(get_prev_line(i-2, input_lines))]
                }

            pass
        elif 'TRANSLATION' in line:
            in_translation = True
            count += 1
            pass

    return data


def insert_data_into_db():
    data = generate_data()
    list_of_values = prepare_data(data)

    response = []
    for row in list_of_values:
        result = yield from (GitaRepository.insert_chapter_text(row))
        response.append(result)
    data = {"result_count": len(response)}
    return data


class EchoClientProtocol(asyncio.Protocol):

    def __init__(self):
        self.request_future = {}
        pass

    def connection_made(self, transport):
        self._transport = transport
        #transport.write("started_echo".encode())
        print_log("connection_made")

    def send(self, message):
        request_id = unique_hex()
        future = Future()
        self.request_future[request_id] = future
        packet = {
            "request_id": request_id,
            "message": message
        }
        data_sent = json.dumps(packet)
        print_log("send -> " + data_sent)
        self._transport.write(data_sent.encode())
        return future

    def data_received(self, data):
        data = data.decode()
        data_recieved = json.loads(data)
        request_id = data_recieved["request_id"]
        future = self.request_future.pop(request_id)
        if future:
            future.set_result(data_recieved["message"])
        print_log(data_recieved)

    def connection_lost(self, exc):
        print_log('The server closed the connection')
        print_log('Stop the event loop')
        self._transport = None


ignore_word_reg = re.compile('.*[^a-zA-Z?:\n\r].*', re.DOTALL)


def remove_useless_word(text:str):
    txt = []
    text = text.replace('\n',' ').replace('\r',' ').replace(':',' ').replace('?',' ')
    #print(text.split(' '))
    for word in text.split(' '):
        if len(word)>1 and not ignore_word_reg.match(word):
            txt.append(word)
    return ' '.join(txt)


def generate_keywords():
    global max_scores
    if max_scores is None:
        max_scores = yield from GitaRepository.get_max_scores()
    result = yield from GitaRepository.get_all_data(offset=0, limit=1000)
    global protocol
    if not protocol:
        _, protocol = yield from asyncio.get_event_loop().create_connection(lambda: EchoClientProtocol(), '127.0.0.1', 8887)

    tag_word = defaultdict(set)
    for data in result:
        translation = data['translation']

        keyword_score_dict = defaultdict(lambda : [0, 0])
        if translation:
            #print(translation)
            translation = remove_useless_word(translation)
            keywords = yield from protocol.send(translation)
            synonym_keywords = []
            for word in keywords:
                syn_li = dictionary.synonym(word[0])
                if syn_li:
                    synonym_keywords += syn_li

            keywords = [stem(keyword[0]) for keyword in keywords]
            for keyword in keywords:
                keyword_score_dict[keyword][0] += 8
                keyword_score_dict[keyword][1] += 1

            keywords = [stem(keyword[0]) for keyword in synonym_keywords]
            for i, keyword in enumerate(keywords):
                keyword_score_dict[keyword][0] += 6
                keyword_score_dict[keyword][1] += 1
        translation = data['purport']
        if translation:
            #print(translation)
            translation = remove_useless_word(translation)
            keywords = yield from protocol.send(translation)
            keywords = [stem(keyword[0]) for keyword in keywords]
            synonym_keywords = []
            for word in keywords:
                syn_li = dictionary.synonym(word[0])
                if syn_li:
                    synonym_keywords += syn_li

            for keyword in keywords:
                keyword_score_dict[keyword][0] += 3
                keyword_score_dict[keyword][1] += 1
            keywords = [stem(keyword[0]) for keyword in synonym_keywords]
            for i, keyword in enumerate(keywords):
                keyword_score_dict[keyword][0] += 2
                keyword_score_dict[keyword][1] += 1

        for keyword, score in keyword_score_dict.items():
            score_value = get_score(score[0], score[1], 0, 0)
            row = {"gita_data_id": data["id"], "keyword": keyword, "sys_score": score[0], "sys_score_count": score[1], "score": score_value}
            yield from GitaRepository.insert_gita_data_keyword(row)

    data = "success"
    return data
    #print(tag_word)


def get_score(sys_score, sys_score_count, user_score, user_score_count):
    global max_scores
    max_sys_score = max_scores[1]
    max_user_score = max_scores[2]

    sys_rating, user_rating = 0.0, 0.0
    if sys_score_count:
        sys_rating = sys_score * sys_score_count
        if sys_rating > max_sys_score:
            max_sys_score = sys_rating
            max_scores[1] = max_sys_score
            asyncio.async(GitaRepository.update_max_score(max_sys_score, 1))
        sys_rating = sys_rating*100/max_sys_score

    if user_score_count:
        user_count_score = (user_score - (user_score_count - user_score))
        user_rating = user_score + (user_count_score if user_count_score > 0 else 0)
        if user_rating > max_user_score:
            max_user_score = user_rating
            max_scores[2] = max_user_score
            asyncio.async(GitaRepository.update_max_score(max_user_score, 2))
        user_rating = (user_rating*100/max_user_score) if max_user_score>0 and user_rating>=0 else 0

    score = sys_rating * 0.5 + user_rating*0.5
    return score

protocol = None


def search_problem_solution(request: Request):
    payload = yield from request.json()
    question = payload['question']
    user_id = request.GET.get('user_id', 'guest')
    print(question, user_id)
    global protocol

    if not protocol:
        _, protocol = yield from asyncio.get_event_loop().create_connection(lambda: EchoClientProtocol(), '127.0.0.1', 8887)

    text = remove_useless_word(question)
    keywords = yield from protocol.send(text)
    keywords = [stem(keyword[0]) for keyword in keywords]
    gita_id = yield from GitaRepository.search_best_gita_data(keywords)
    if gita_id:
        data = yield from GitaRepository.get_gita_data_by_id(gita_id)
        result = yield from GitaRepository.insert_question(question, user_id, gita_id, keywords)
        data['question_id'] = result['id']
        #print_gita_data(data)
        response = yield from GitaRepository.get_gita_keywords_by_id(gita_id, keywords)
        #print (response)
        data["keywords"] = response
    else:
        data = {"error": "No result found"}
    return web.Response(body=json.dumps(data).encode())


def update_score_using_feedback(question_id, user_id, feedback):
    data = yield from GitaRepository.save_feedback(question_id, user_id, feedback)
    if data:
        keywords = data['extra_data']
        gita_id = data['gita_id']
        if int(feedback) == 1:
            user_score = 1
        else:
            user_score = 0
        yield from GitaRepository.update_user_score(gita_id, keywords, user_score)
        keyword_data = yield from GitaRepository.get_gita_keywords_by_id(gita_id, keywords)
        keyword_data_dict = {}
        for keyword_obj in keyword_data:
            keyword_data_dict[keyword_obj['keyword']] = keyword_obj
        for keyword in keywords:
            if keyword in keyword_data_dict:
                score = get_score(keyword_data_dict[keyword]['sys_score'], keyword_data_dict[keyword]['sys_score_count'],
                                  keyword_data_dict[keyword]['user_score'], keyword_data_dict[keyword]['user_score_count'])
                yield from GitaRepository.update_score(gita_id, keyword, score)
            else:
                score = get_score(0, 0, user_score, 1)
                row = {"gita_data_id": data["id"], "keyword": keyword, "user_score": user_score,
                       "user_score_count": 1, "score": score}
                yield from GitaRepository.insert_gita_data_keyword(row)


def save_feedback(request: Request):
    question_id = request.GET['question_id']
    user_id = request.GET['user_id']
    feedback = request.GET['feedback']
    data = None
    if id:
        yield from update_score_using_feedback(question_id, user_id, feedback)
        data = {"message": "Feedback Accepted"}
    if not data:
        data = {"error": "Not saved"}
    return web.Response(body=json.dumps(data).encode())


def print_gita_data(data:dict):
    print("CHAPTER {} TEXT {}\n".format(data["chapter_num"],data["text_num"]))
    print(data["text"] + "\n")

    print("TRANSLATION\n")
    print(data["translation"] + "\n")

    print("PURPORT\n")
    print(data["purport"] + "\n")


loop = asyncio.get_event_loop()


def index_page(request: Request):
    html = '<div> Welcome to gita search </div> <input type=text /> <input type=button/>'
    return web.Response(body=json.dumps(html).encode(), content_type='text/html; charset=utf-8')


max_scores = None


def init():
    GitaRepository.connect()
    global max_scores
    max_scores = yield from GitaRepository.get_max_scores()
    app = web.Application(loop=loop)
    app.router.add_route('POST', '/search', search_problem_solution)
    app.router.add_route('GET', '/feedback', save_feedback)
    app.router.add_route('GET', '/', index_page)

    srv = yield from loop.create_server(app.make_handler(),'0.0.0.0', 8080)
    print("Server started at http://0.0.0.0:8080")
    return srv

if __name__ == '__main__':
    #insert_data_into_db()
    loop.run_until_complete(init())
    #asyncio.get_event_loop().run_until_complete(main_task())
    loop.run_forever()
